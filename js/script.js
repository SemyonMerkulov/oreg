$(document).ready(function(){

	$('.popular__show').click(function(e){
		e.preventDefault();
		$('.popular__col').removeClass('hide');
	});

	$('.left-menu-btn').click(function(e){
		e.preventDefault();
		var headerHeight = $('header').outerHeight();
		var browserHeight = document.documentElement.clientHeight;
		var height = browserHeight - headerHeight;

		if($('.aside').hasClass('open')){

			$('.aside').animate({left: '-200%'}, 200).removeClass('open');
			$('body').css('overflow','visible');
		//	$('.aside').hide();
		}
		else {
			$('body').css('overflow','hidden');

			$('.aside').show();
			$('.aside').animate({left: '0', height: height, top: headerHeight}, 200).addClass('open');
		}	

	});

	$('.cart-fixed__header').click(function(){
		var headerHeight = $(".cart-fixed__header").outerHeight();
		var contentHeight = $(".cart-fixed__content").outerHeight();
		var orderHeight = $(".cart-fixed__order").outerHeight();
		var blockHeight = orderHeight + contentHeight + headerHeight + 12;
		console.log(blockHeight);
		if($('.cart-fixed').hasClass('open')){
			$('.cart-fixed').removeClass('open').stop().animate({height: headerHeight, right: '-150px'}, 200);
		}
		else {
			$('.cart-fixed').addClass('open').stop().animate({right: '0'}, 200).height('auto');
		}

	})

	 $(document).click(function(event) {
	 var headerHeight = $(".cart-fixed__header").outerHeight();
    if ($(event.target).closest(".cart-fixed.open").length) return;
    $(".cart-fixed.open").removeClass('open').animate({height: headerHeight, right: '-150px'}, 200);
    event.stopPropagation();
  		});
	

	$('.modal-info__close').click(function(e){
		e.preventDefault();
		$(this).parent().hide();
		$('.overlay').fadeOut();

	});

	$('.prod__add-remove').each(function(){
		$(this).click(function(e){
		e.preventDefault();
		$(this).parent().remove();
		});
	});

	$(".ask-form__file").change(function() {
	    var fileName = $(this).val().split('/').pop().split('\\').pop();
	    $('.ask-form__filename').text(fileName);
	});

	$("#form-phone").mask("+7-999-9999999", {placeholder: "_"});

});